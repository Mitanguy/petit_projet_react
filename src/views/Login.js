import React, {useEffect} from 'react';
import {Link} from 'react-router-dom'
import SignIn from '../components/signin'
import axios from "axios";
import styled from 'styled-components'


const submit = (e,formState, setErrorMessage,history) => {
    e.preventDefault();
    if (!formState.username || !formState.password){
        setErrorMessage("les champs ne doivent pas etre vide");
        return
    }

    axios({
        method: 'POST',
        url:'https://easy-login-api.herokuapp.com/users/login',
        data:{
            username: formState.username,
            password: formState.password
        }
    }).then(res => {
        localStorage.setItem("token",res.headers['x-access-token']);
        history.push('/home')
    }).catch(err => {
        setErrorMessage('une érreur est survenue réessayer plus tard');
        console.log(err)
        })
    };
    const Login = ({history}) => {
        useEffect(()=> {
            const token = localStorage.getItem('token');
            if (token){
                history.push('/Home')
            }
        },[]);
    return (
        <LoginContainer>
            <SignIn submit={submit}></SignIn>
        </LoginContainer>
    );
};


const LoginContainer = styled.div`
    width:100%;
    height: 100%;
    background: #222;
    position: absolute
`

export default Login;