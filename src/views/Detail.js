import React, {useEffect, useState} from 'react';
import privateKey from "../.private";
import axios from "axios";
import Comment from '../components/Comment'
import MovieDetail from "../components/MovieDetail";
import styled from "styled-components"



const Details = (props) => {
    const [moviesDetail,setMoviesDetail] = useState({});
    const [comments,setComments] = useState([]);
    const urlID = props.match.params.id

    const comment = (e,formState, setErrorMessage,props) => {
        e.preventDefault();
        if (!formState.Comment){
            setErrorMessage("les champs ne doivent pas etre vide");
        }else{
            setErrorMessage("");
            const currentComment = localStorage.getItem(`comment_${urlID}`)
                ? JSON.parse(localStorage.getItem(`comment_${urlID}`))
                : [];
            const commentData = {
                comment: formState.Comment,
            }
            currentComment.push(commentData);
            localStorage.setItem(`comment_${urlID}`, JSON.stringify(currentComment))
            setComments(currentComment)
        }
    };

    useEffect(()=> {
        const generalUrl = `https://api.themoviedb.org/3/movie/${props.match.params.id}`;
        const keyApi = `${privateKey}`;
        setComments(localStorage.getItem(`comment_${urlID}`)
            ? JSON.parse(localStorage.getItem(`comment_${urlID}`))
            : []);
        axios({
            method:'GET',
            url: generalUrl,
            params:{
                api_key:keyApi,
            }
        }).then(res => {
            setMoviesDetail(res.data);
        }).catch(err => {
            console.log(err)
        })
    },[]);

    return (
        <div>
            <MovieDetail url={`${moviesDetail.poster_path}`} id={moviesDetail.id} desc={moviesDetail.overview} name={moviesDetail.title}/>

                <Comment submit={comment}></Comment>

            {comments.map(comments => (
                <CommentContainer>
                        <p>{comments.comment}</p>
                </CommentContainer>
            ))}

        </div>
    );
};


const CommentContainer = styled.div`
    padding: 0 !important;
    width: 22rem;
    margin:14px;
    border-radius: 10px;
    box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 4px 15px 0 rgba(0, 0, 0, 0.19);
    margin: 14px auto;
    padding:15px;
    display: flex;
    img{
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    }
    h2,p{
    text-align: left;
    margin: 15px 30px;
    }
`
export default Details;