import React from 'react';
import styled from "styled-components";

const Header = () => {
    return (
        <HeaderApp>
            <TitreApp>AlloCiné</TitreApp>
        </HeaderApp>
    );
};


const HeaderApp = styled.header`
width: 100%;
height: 50px;
background-color: #fecc00;
position: fixed;
top:0px;
left:0px;
z-index:10;
`;

const TitreApp = styled.h1`
color: #222;
font-size:20px;
text-align:center
`

export default Header;