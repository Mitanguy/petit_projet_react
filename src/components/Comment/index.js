import React, {useState,useEffect}from 'react';
import styled from 'styled-components'

const Comment = ({submit}) => {
    const [formState, setFormState] = useState({Comment: ''})
    const [errorMessage, setErrorMessage] = useState('')
    return (
        <StyledForm onSubmit={e => submit(e,formState, setErrorMessage)}>
            <StyledSpan>Poster un commentaire</StyledSpan>
            <textarea name="comment" id="" cols="30" rows="10" onChange={e => setFormState({...formState,Comment: e.target.value})}></textarea>
            <SignInInput  type="Submit" />
            <StyledSpan>{errorMessage}</StyledSpan>
        </StyledForm>
    );
};

const StyledSpan = styled.span`
    color:#000;
    margin-bottom:12px
`

const StyledForm = styled.form`
 display: flex;
 flex-direction: column;
 align-items: center;
 justify-content: center;
`

const SignInInput = styled.input`
    margin:6px 0px;
    border-radius: 20px;
    border:none;
    background-color:#222;
    height:30px;
    color:#fff;
    padding: 0px 6px
    `

export default Comment;