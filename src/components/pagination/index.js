import React, {useState,useEffect} from 'react';
import styled from "styled-components"

const pageCreator = (pageNumber,setCurrentPage) =>{
    let pagesElement = [];

    for (let i = 0; i <= pageNumber; i++){
        pagesElement.push(<span onClick={()=> setCurrentPage(i)}>{i}</span>)
    }
    return pagesElement
}

const Pagination = ({total, setCurrentPage}) => {
    const [pages, setPages] = useState(0);
    useEffect(()=>{
        const numberPages = total;
        console.log('pagination -> numberpages', numberPages);
        setPages(numberPages)
    },[total]);
    if (total === 0 ) return null;
    return (
            <PaginationContainer>{pageCreator(pages,setCurrentPage)}</PaginationContainer>
    );
};

const PaginationContainer = styled.div`
display: flex;
background-color: #000;
color:#fff;
flex-wrap: wrap
`;

export default Pagination;