import React from 'react';
import MovieTitle from "../MovieTitle/"
import MovieImg from "../MovieImg/"
import styled from "styled-components"
import MovieDesc from "../MovieDesc";

const MovieDetail = (props) => {


    return (
        <CaracterContainer >
            <MovieImg url={props.url} alt=""/>
            <MovieTitle name={props.name}></MovieTitle>
            <MovieDesc desc={props.desc}/>
        </CaracterContainer>
    );

}

const CaracterContainer = styled.div`
    padding: 0 !important;
    width: 22rem;
    margin:14px;
    border-radius: 10px;
    box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 4px 15px 0 rgba(0, 0, 0, 0.19);
    margin: 14px 10px;
    display: inline-block;
    img{
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    }
    h2,p{
    text-align: left;
    margin: 15px 10px;
    }
    
    a{
    color: #5e5c5c;
    text-decoration:none
    }

`

export default MovieDetail;
