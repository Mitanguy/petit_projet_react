import React, {useState, useEffect} from 'react';
import MovieRow from "../MovieRow";


const FavoriteMovie = () => {
    const [fav,setFav] = useState([]);
    const [errorMessage,setErrorMessage] = useState("");


    useEffect(()=> {
    setFav(localStorage.getItem(`favorite`)
        ? JSON.parse(localStorage.getItem(`favorite`))
        : []);
    if (fav.length === 0){
        setErrorMessage("vous n'avez pas de favoris");
    }else{
        setErrorMessage("");
    }
    },[]);
    return (
        <div>
            <h1>Favoris</h1>
            {fav.map(movies => (
                <MovieRow url={`${movies.img}`}  key={movies.id} id={`${movies.id}`} link={`${movies.link}`} favorite={true} setfav={setFav}/>
            ))}
            <p>{errorMessage}</p>
        </div>
    );
};

export default FavoriteMovie;