import React from 'react';
import styled from "styled-components";
import Nav from "../Nav";

const Footer = () => {
    return (
        <FooterApp>
            <Nav/>
        </FooterApp>
    );
};

const FooterApp = styled.footer`
width: 100%;
height: 50px;
background-color: rgba(0,0,0,0.8);
position: fixed;
bottom:0px;
left:0px
`;


export default Footer;