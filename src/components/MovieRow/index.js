import React from 'react';
import MovieTitle from "../MovieTitle/"
import MovieImg from "../MovieImg/"
import {Link} from "react-router-dom"

import styled from "styled-components"

const MovieRow = (props) => {

    const handleFavorite = (props) => {
        const currentFavorite = localStorage.getItem("favorite")
            ? JSON.parse(localStorage.getItem('favorite'))
            : [];
        const isPresent = currentFavorite.map(e => e.id).indexOf(props.id);

        if(isPresent === -1){
            currentFavorite.push(props);
            localStorage.setItem('favorite', JSON.stringify(currentFavorite))
        }
    }


    const deleteFavorite = (prop) => {
        let isPresentFavorite = JSON.parse(localStorage.getItem('favorite')) || [];

        let indexOfExistingProduct = isPresentFavorite.findIndex(
            (el) => el.id === prop.id
        );
        isPresentFavorite.splice(indexOfExistingProduct, 1);
        localStorage.setItem('favorite',JSON.stringify(isPresentFavorite));
        props.setfav(isPresentFavorite)

    }

    let container;
    if(props.favorite){

        container = <div>
            <Link to={props.link}>
                <MovieImg url={props.url} alt="vous n'avez pas de connection veuillez réessayer"/>
                <MovieTitle name={props.name}></MovieTitle>
            </Link>
            <ButtonMovie onClick={() => deleteFavorite({id: props.id})}> delete favorite</ButtonMovie>
        </div>;
    }else{

        container=
            <div>
            <Link to={props.link}>
                <MovieImg url={props.url} alt=""/>
                <MovieTitle name={props.name}></MovieTitle>
            </Link>
            <ButtonMovie onClick={() => handleFavorite({id: props.id,title: props.name,img: props.url,link: props.link})}> add to favorite</ButtonMovie>
            </div>;
    }

    return (
        <CaracterContainer key={props.id}>
            {container}
        </CaracterContainer>

    );

}

const CaracterContainer = styled.div`
    padding: 0 !important;
    width: 10rem;
    margin:14px;
    border-radius: 10px;
    box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.2), 0 4px 15px 0 rgba(0, 0, 0, 0.19);
    margin: 14px 10px;
    display: inline-block;
    img{
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    }
    
    a{
    color: #5e5c5c;
    text-decoration:none
    }

`
const ButtonMovie = styled.button`
    margin-bottom: 15px

`

export default MovieRow;
