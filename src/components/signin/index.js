import React, {useState,useEffect}from 'react';
import styled from 'styled-components'
import {useHistory} from 'react-router-dom'

const SignIn = ({submit}) => {
    const [formState, setFormState] = useState({username: '', password: ''})
    const [errorMessage, setErrorMessage] = useState('')
    const history = useHistory();
    console.log(history);



    useEffect(()=>{
        console.log(formState)

    })
    return (
        <StyledForm onSubmit={e => submit(e,formState, setErrorMessage,history)}>
            <StyledSpan>Sign in</StyledSpan>
            <SignInInput name="username" placeholder="Username" onChange={e => setFormState({...formState,username: e.target.value})} type="text"></SignInInput>
            <SignInInput placeholder="password" onChange={e => setFormState({...formState,password: e.target.value})} type="password"></SignInInput>
            <SignInInput  type="Submit"></SignInInput>
            <StyledSpan>{errorMessage}</StyledSpan>
        </StyledForm>
    );
};

const StyledSpan = styled.h1`
    margin: 20px auto;
    color: #fff;
    font-size:20px;
`

const StyledForm = styled.form`
 display: flex;
 flex-direction: column;
 align-items: center;
 width:70%;
 margin: 0 auto;
 position:relative;
 top: 30%
`

const SignInInput = styled.input`
    margin:8px 0px;
    border-radius: 12px;
    padding: 8px 15px;
	border-radius: 15px;
	background-color: #fff;
    height:30px;
    color:#222;
    padding: 0px 6px;
    width:100%
    `

export default SignIn;