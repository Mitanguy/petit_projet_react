import React from 'react';
import {Link} from "react-router-dom";
import styled from "styled-components";

const Nav = () => {
    return (
        <NavContainer>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/Favorite">Favorite</Link>
                </li>
            </ul>
        </NavContainer>
    );
};

const NavContainer = styled.nav`
    width:100%;
    ul{
    display: flex;
    justify-content: space-between;
    padding:0;
        li{
         text-decoration:none;
         list-style:none;
         margin: 0 auto;
            a{
            text-decoration:none;
            color:#FFF
            }
        }
    }
`

export default Nav;