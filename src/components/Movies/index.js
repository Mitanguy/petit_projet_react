import React, { Component, useEffect,useState } from "react";
import MovieRow from "../MovieRow"
import axios from 'axios'
import privateKey from "../../.private";

const Movies = () => {

    const [moviesLists,setMoviesList] = useState([])
    const [currentPage, setCurrentPage] = useState(1);
    const [errorMessage, setErrorMessage] = useState("");
    const [total, setTotal] = useState(0);

    useEffect(()=> {
        const generalUrl = "https://api.themoviedb.org/3/movie/popular";
        const keyApi = `${privateKey}`;

        axios({
            method:'GET',
            url: generalUrl,
            params:{
                api_key:keyApi,
                page: currentPage
            }
        }).then(res => {
            setMoviesList(res.data.results);
            setTotal(res.data.total_pages)
        }).catch(err => {
            console.log(err)
            setErrorMessage("vous n'etes pas connecté veuillez réessayer plus tard")
        })
    },[currentPage])

    return (
        <div>
            {moviesLists.map(movies => (
                <MovieRow url={`${movies.poster_path}`}  key={movies.id} link={`details/${movies.id}`} id={movies.id}/>

            ))}
            <p>{errorMessage}</p>
        </div>
    );

}

export default Movies;