import React from 'react';

const MovieDesc = (props) => {


    return (
        <div>
            <p>{props.desc}</p>
        </div>
    );

}

export default MovieDesc;
