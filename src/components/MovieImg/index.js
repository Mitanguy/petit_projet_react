import React from 'react';
import styled from "styled-components"
import PropTypes from 'prop-types'

const MovieImg = (props) => {




    return (
        <div>
            <MovieImages width={200} src={`https://image.tmdb.org/t/p/w500${props.url}`} alt=""/>
        </div>
    );

};

MovieImg.propTypes = {
    url: PropTypes.string
}

const MovieImages = styled.img`
width: 100%;
height: auto;
space-between: 10px
`;

export default MovieImg;
