import React,{useState,useEffect} from 'react';
import {Route,BrowserRouter as Router,Redirect, Switch} from 'react-router-dom'
import Login from '../views/Login'
import Home from '../views/Home'
import Details from "../views/Detail";
import Favorite from "../views/Favorite";
import Nav from "../components/Nav";
import PrivateRoute from '../utils/privateRoute'
import styled from "styled-components"
import Footer from "../components/Footer";
import Header from "../components/Header";

const Routes = () => {
    return (

        <ContentContainer>
            <Header/>
        <Router>
            <Switch>
                <Route exact path="/" component={Login}/>
                <PrivateRoute path="/Home" component={Home}/>
                <PrivateRoute path="/details/:id" component={Details}/>
                <PrivateRoute path="/Favorite" component={Favorite}/>
                <Redirect to="/"/>
            </Switch>
        </Router>
        </ContentContainer>

    );
};

const ContentContainer = styled.div`
    padding-top:50px;
    padding-bottom:50px;
    width:100%;
    overflow:hidden

`

export default Routes;