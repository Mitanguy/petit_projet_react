import React from 'react';
import {Redirect, Route} from 'react-router-dom'
import Footer from "../components/Footer";

const PrivateRoute = ({component: Component, ...rest}) => {
    return (
       <Route
           {...rest}
           render={props =>
               localStorage.getItem('token') ? (
                   <div>
                   <Component {...props}/>
                   <Footer/>
                   </div>
                   ) : (
                   <Redirect to="/"/>
               )
       }
        ></Route>
    );
};

export default PrivateRoute;